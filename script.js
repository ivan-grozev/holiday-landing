var categories = [
    {
        "id": 1,
        "name": "Бюджетни лаптопи",
        "image": "/ckeditor_assets/pictures/2950/content_holiday-banner-1-2-budget-laptops.png",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut mauris eu arcu dictum pulvinar. Curabitur felis quam, porttitor in interdum a, varius vel nisl.",
        "filters": [

            {
                "name": "Бюджетни лаптопи",
                "products": [125131]
            }
        ]
    },
    {
        "id": 2,
        "name": "Топ модели лаптпопи",
        "image": "/ckeditor_assets/pictures/2951/content_holiday-banner-2-budget-laptops.png",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut mauris eu arcu dictum pulvinar. Curabitur felis quam, porttitor in interdum a, varius vel nisl.",
        "filters": [
            {
                "name": "Топ модели лаптопи",
                "products": [142981]
            }
        ]
    },
    {
        "id": 3,
        "name": "Геймърски лаптопи",
        "image": "/ckeditor_assets/pictures/2952/content_holiday-banner-3-budget-laptops.png",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut mauris eu arcu dictum pulvinar. Curabitur felis quam, porttitor in interdum a, varius vel nisl.",
        "filters": [
            {
                "name": "Геймърски лаптопи",
                "products": [134973]
            }
        ]
    },
    {
        "id": 4,
        "name": "Бюджетни компютри",
        "image": "/ckeditor_assets/pictures/2953/content_holiday-banner-4-budget-pc.png",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut mauris eu arcu dictum pulvinar. Curabitur felis quam, porttitor in interdum a, varius vel nisl. Quisque quis viverra dolor. Praesent in eleifend tortor, accumsan bibendum justo. Sed odio diam, blandit quis lorem in, volutpat tincidunt tellus.",
        "filters": [
            {
                "name": "Бюджетни компютри",
                "products": [144063]
            }
        ]
    }
    ,
    {
        "id": 5,
        "name": "Топ модели компютри",
        "image": "/ckeditor_assets/pictures/2956/content_holiday-banner-5-budget-pc.png",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut mauris eu arcu dictum pulvinar. Curabitur felis quam, porttitor in interdum a, varius vel nisl. Quisque quis viverra dolor. Praesent in eleifend tortor, accumsan bibendum justo. Sed odio diam, blandit quis lorem in, volutpat tincidunt tellus.",
        "filters": [
            {
                "name": "1000 - 2000",
                "products": [144151]
            },
            {
                "name": "2000 - 3000",
                "products": [143522, 144554]
            },
            {
                "name": "3000 - 4000",
                "products": [143522, 144554]
            }
        ]
    }
    ,
    {
        "id": 6,
        "name": "Геймърски компютри",
        "image": "/ckeditor_assets/pictures/2954/content_holiday-banner-4-budget-laptops.png",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut mauris eu arcu dictum pulvinar. Curabitur felis quam, porttitor in interdum a, varius vel nisl. Quisque quis viverra dolor. Praesent in eleifend tortor, accumsan bibendum justo. Sed odio diam, blandit quis lorem in, volutpat tincidunt tellus.",
        "filters": [
            {
                "name": "1000 - 2000",
                "products": [144151]
            },
            {
                "name": "2000 - 3000",
                "products": [143522, 144554]
            },
            {
                "name": "3000 - 4000",
                "products": [143522, 144554]
            }
        ]
    }
    ,
    {
        "id": 7,
        "name": "Монитори",
        "image": "/ckeditor_assets/pictures/2957/content_holiday-banner-1-display.png",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut mauris eu arcu dictum pulvinar. Curabitur felis quam, porttitor in interdum a, varius vel nisl. Quisque quis viverra dolor. Praesent in eleifend tortor, accumsan bibendum justo. Sed odio diam, blandit quis lorem in, volutpat tincidunt tellus.",
        "filters": [
            {
                "name": "1000 - 2000",
                "products": [144151]
            },
            {
                "name": "2000 - 3000",
                "products": [143522, 144554]
            },
            {
                "name": "3000 - 4000",
                "products": [143522, 144554]
            }
        ]
    },
    {
        "id": 8,
        "name": "Аксесоари",
        "image": "/ckeditor_assets/pictures/2962/content_holiday-banner-1-aksesoari-2.png",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut mauris eu arcu dictum pulvinar. Curabitur felis quam, porttitor in interdum a, varius vel nisl.",
        "filters": [
            {
                "name": "Аксесоари",
                "products": [144151]
            }
        ]
    }
    ,
    {
        "id": 9,
        "name": "Tелевизори",
        "image": "/ckeditor_assets/pictures/2958/content_holiday-banner-1-tv-1.png",
        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut mauris eu arcu dictum pulvinar. Curabitur felis quam, porttitor in interdum a, varius vel nisl. Quisque quis viverra dolor. Praesent in eleifend tortor, accumsan bibendum justo. Sed odio diam, blandit quis lorem in, volutpat tincidunt tellus.",
        "filters": [
            {
                "name": "Телевизори",
                "products": [142330]
            }
        ]
    }
]

var laptops = [
    /*Tower PC START*/
    {
        "categoryItem": "pc",
        "name": "PIC Intel Game&Home i3 3Gen",
        "id": 144063,
        "part_num": 1,
        "colors": [
            {
                color: 'black',
                text: 'черен',
                id: 144063,
                img: "https://www.pic.bg/image/show?f=1572252971.jpg&w=580&h=430"
            }
        ],
        "url": "https://www.pic.bg/nastolen-kompyutyr-pic-intel-gamehome-i3-3gen-pc02helli3geni33220-144063",
        "img": "https://www.pic.bg/image/show?f=1572252971.jpg&w=580&h=430",
        "cpu": "Intel® Core™ i3-3220 Processor 3.30 GHz, 3 MB cache",
        "ram": "8GB 1600 MHz DDR3",
        "display": "15.6\" (39.62 cm) HD Anti-Glare 1366x768 матов",
        "vga": "Intel® HD Graphics 2500",
        "hdd": "1000GB 7.2krpm SATA",
        "ssd": "Без SSD",
        "variations": [
            {
                ssd: 144063,
                price: 419,
                label: "Базов",
                label_ram: "8GB 1600MHz Тип: DDR3",
                label_ssd: "1000GB 7.2k rpm",
                link: "/144063",
                colors: [
                    {
                        color: "black",
                        id: 144063
                    }
                ]
            },
            {
                ssd: 144066,
                price: 479,
                label: "120GB SSD",
                label_ram: "8GB 1600MHz Тип: DDR3",
                label_ssd: "120GB SSD",
                link: "/144066",
                colors: [
                    {
                        color: "black",
                        id: 144066
                    }
                ]
            },
            {
                ssd: 144064,
                price: 499,
                label: "8GB RAM",
                label_ram: "8GB 1600MHz Тип: DDR3",
                label_ssd: "240GB SSD",
                link: "/144064",
                colors: [
                    {
                        color: "black",
                        id: 144064
                    }
                ]
            }, {
                ssd: 144067,
                price: 609,
                label: "8GB RAM и 500GB SSD",
                label_ram: "8GB 1600MHz Тип: DDR3",
                label_ssd: "500GB SSD",
                link: "/144067",
                colors: [
                    {
                        color: "black",
                        id: 144067
                    }
                ]
            }
        ]
    },
    /*Tower PC END*/

    /*LAPTOPS START*/
    {
        "categoryItem": "laptop",
        "name": "Lenovo IdeaPad S145-IWL",
        "id": 125131,
        "part_num": 1,
        "colors": [
            {
                color: 'gray',
                text: 'сив',
                id: 125131,
                img: "https://www.pic.bg/image/show?f=1566544099.jpg&w=580&h=430"
            }
        ],
        "url": "https://www.pic.bg/laptop-lenovo-ideapad-s145-iwl-81mv0050rm-125131",
        "img": "https://www.pic.bg/image/show?f=1566544099.jpg&w=580&h=430",
        "cpu": "Intel® Celeron® 4205U 1.8 GHz, 2MB cache",
        "ram": "4GB 2133MHz Тип: DDR4",
        "display": "15.6\" (39.62 cm) HD Anti-Glare 1366x768 матов",
        "vga": "Intel® UHD Graphics 610",
        "hdd": "Без HDD",
        "ssd": "120GB SATA 2.5 SSD",
        "variations": [
            {
                ssd: 125131,
                price: 429,
                label: "Базов",
                label_ram: "4GB 2133MHz Тип: DDR4",
                label_ssd: "Без HDD",
                link: "/125131",
                colors: [
                    {
                        color: "black",
                        id: 125131
                    }
                ]
            },
            {
                ssd: 138277,
                price: 489,
                label: "120GB SSD",
                label_ram: "4GB 2133MHz Тип: DDR4",
                label_ssd: "240GB SSD",
                link: "/138277",
                colors: [
                    {
                        color: "black",
                        id: 138277
                    }
                ]
            },
            {
                ssd: 136260,
                price: 479,
                label: "8GB RAM",
                label_ram: "8GB 2133MHz Тип: DDR4",
                label_ssd: "120GB SSD",
                link: "/136260",
                colors: [
                    {
                        color: "black",
                        id: 136260
                    }
                ]
            }, {
                ssd: 138293,
                price: 609,
                label: "8GB RAM и 500GB SSD",
                label_ram: "8GB 2133MHz Тип: DDR4",
                label_ssd: "500GB SSD",
                link: "/138293",
                colors: [
                    {
                        color: "black",
                        id: 138293
                    }
                ]
            }
        ]
    },
    {
        "categoryItem": "laptop",
        "name": "Lenovo Legion Y540-15IRH-PG0",
        "id": 134973,
        "part_num": 1,
        "colors": [
            {
                color: 'black',
                text: 'Черен',
                id: 134973,
                img: "/ckeditor_assets/pictures/2769/content_03_legion_y540_product_photography_15inch_hero_front_facing_left.png"
            }
        ],
        "url": "https://www.pic.bg/laptop-lenovo-legion-y540-15irh-pg0-81sy0057bm-134973",
        "img": "/ckeditor_assets/pictures/2769/content_03_legion_y540_product_photography_15inch_hero_front_facing_left.png",
        "cpu": "Intel® Core™ i5-9300H 2.40 GHz up to 4.10 GHz , 8MB cache",
        "ram": "8GB 2666MHz (1x8GB) Тип: DDR4",
        "display": "15.6\" (39.62 cm) FullHD IPS Anti-Glare 1920x1080 матов",
        "vga": "NVIDIA® GeForce® GTX 1650 (4GB GDDR5)",
        "hdd": "Без HDD",
        "ssd": "256GB PCIe NVMe SSD",
        "add_windows": true,
        "windows": "+ Windows 10 Pro - 120 лв.",
        "variations": [
            {
                ssd: 256,
                price: 1599,
                label: "Базов",
                label_ram: "8GB Тип: DDR4",
                link: "/134973",
                colors: [
                    {
                        color: "black",
                        id: 134973
                    }
                ]
            },
            {
                ssd: 500,
                price: 1709,
                label: "500GB SSD",
                label_ssd: "500GB PCIe NVMe SSD",
                link: "/138541",
                colors: [
                    {
                        color: "black",
                        id: 138541
                    }
                ]
            },
            {
                ssd: 501,
                price: 1809,
                label: "16GB RAM и 500GB SSD",
                label_ram: "16GB Тип: DDR4",
                label_ssd: "500GB PCIe NVMe SSD",
                link: "/138545",
                colors: [
                    {
                        color: "black",
                        id: 138545
                    }
                ]
            },
            {
                ssd: 502,
                price: 1919,
                label: "16GB RAM и 1TB SSD",
                label_ram: "16GB Тип: DDR4",
                label_ssd: "1TB PCIe NVMe SSD",
                link: "/138547",
                colors: [
                    {
                        color: "black",
                        id: 138547
                    }
                ]
            }
        ]
    },
    {
        "categoryItem": "laptop",
        "name": "HP ProBook 450 G6",
        "id": 142981,
        "part_num": 1,
        "colors": [
            {
                color: 'gray',
                text: 'сив',
                id: 142981,
                img: "https://www.pic.bg/image/show?f=1574933111.jpg&w=580&h=430"
            }
        ],
        "url": "https://www.pic.bg/laptop-hp-probook-450-g6-8mg39ea120ssd-900142985",
        "img": "https://www.pic.bg/image/show?f=1574933111.jpg&w=580&h=430",
        "cpu": "Intel® Core™ i5-8265U Processor 1.60GHz - 3.90GHz, 6MB cache (4-ядрен)",
        "ram": "8GB 2400MHz Тип: DDR4",
        "display": "15.6\" (39.62 cm) FullHD IPS Anti-Glare 1920x1080 матов",
        "vga": "NVIDIA® GeForce® MX250 (2GB GDDR5)",
        "hdd": "1000GB 5.4k rpm",
        "ssd": "Без SSD",
        "variations": [
            {
                ssd: 142981,
                price: 1269,
                label: "Базов",
                label_ram: "8GB 2400MHz Тип: DDR4",
                label_ssd: "Без SSD",
                link: "/142985",
                colors: [
                    {
                        color: "gray",
                        id: 142981
                    }
                ]
            },
            {
                ssd: 142985,
                price: 1319,
                label: "120GB SSD",
                label_ram: "8GB 2400Hz Тип: DDR4",
                label_ssd: "120GB SSD",
                link: "/142985",
                colors: [
                    {
                        color: "gray",
                        id: 142985
                    }
                ]
            },
            {
                ssd: 142989,
                price: 1449,
                label: "8GB RAM",
                label_ram: "8GB 2400MHz Тип: DDR4",
                label_ssd: "500GB SSD",
                link: "/142989",
                colors: [
                    {
                        color: "gray",
                        id: 142989
                    }
                ]
            }, {
                ssd: 142987,
                price: 1449,
                label: "16GB RAM и 500GB SSD",
                label_ram: "16GB 2400MHz Тип: DDR4",
                label_ssd: "500GB SSD",
                link: "/142987",
                colors: [
                    {
                        color: "gray",
                        id: 142987
                    }
                ]
            }
        ]
    },
    /*LAPTOPS END*/

    /*TV START*/
    {
        "categoryItem": "tv",
        "name": "PIC Game Ultimate",
        "id": 142330,
        "part_num": 1,
        "colors": [
            {
                color: 'black',
                text: 'Черен',
                id: 142330,
                img: "https://www.pic.bg/image/show?f=polycomp/142330-f0fa3a7e5c07ec611403dd241a90546e.jpg&w=580&h=430"
            }
        ],
        "url": "https://www.pic.bg/televizor-samsung-32-32n4003-hd-led-tv-ue32n4003akxxh-142330",
        "img": "https://www.pic.bg/image/show?f=polycomp/142330-f0fa3a7e5c07ec611403dd241a90546e.jpg&w=580&h=430",
        "cpu": "32\" (81.28 cm)",
        "ram": "1366x768",
        "display": "200 PQI",
        "vga": "LED",
        "hdd": "Dolby Digital Plus",
        "ssd": "LED",
        "variations": [
            {
                ssd: 2322,
                price: 339,
                label: "Базов",
                label_ram: "8GB 2400MHz Тип: DDR4",
                label_ssd: "Без SSD",
                link: "/142330",
                colors: [
                    {
                        color: "black",
                        id: 144554
                    }
                ]
            }
        ]
    },
    /*TV END*/

    /*Аccessories START*/
    {
        "categoryItem": "pc",
        "name": "PIC Intel Game&Home i3 3Gen",
        "id": 144063,
        "part_num": 1,
        "colors": [
            {
                color: 'black',
                text: 'черен',
                id: 144063,
                img: "https://www.pic.bg/image/show?f=1572252971.jpg&w=580&h=430"
            }
        ],
        "url": "https://www.pic.bg/nastolen-kompyutyr-pic-intel-gamehome-i3-3gen-pc02helli3geni33220-144063",
        "img": "https://www.pic.bg/image/show?f=1572252971.jpg&w=580&h=430",
        "cpu": "Intel® Core™ i3-3220 Processor 3.30 GHz, 3 MB cache",
        "ram": "8GB 1600 MHz DDR3",
        "display": "15.6\" (39.62 cm) HD Anti-Glare 1366x768 матов",
        "vga": "Intel® HD Graphics 2500",
        "hdd": "1000GB 7.2krpm SATA",
        "ssd": "Без SSD",
        "variations": [
            {
                ssd: 144063,
                price: 419,
                label: "Базов",
                label_ram: "8GB 1600MHz Тип: DDR3",
                label_ssd: "1000GB 7.2k rpm",
                link: "/144063",
                colors: [
                    {
                        color: "black",
                        id: 144063
                    }
                ]
            }

        ]
    }
    /*Аccessories END*/
];
var products = [];
var color = {};
var value = {};
var type = 'ssd';

for (var i = 0; i < laptops.length; i++) {
    var laptop = laptops[i];
    /*PC START*/
    if (laptop.categoryItem == 'pc') {
        var variations = laptop.variations;
        var html = '<div class="row product-cust-containers product-container product-container-' + laptop.id + '" style="display:none;">';
        html += "<div class=\"col-xs-12 col-md-7 col-lg-7\">";
        html += "<h3 class='color-heading text-center'>" + laptop.name + "</h3>";
        html += "<img src='" + laptop.img + "' class=\"img-responsive big-img-" + laptop.id + "\">";

        if (laptop.colors.length > 1) {
            html += '<div class="row thumbnail-container text-center">';
            html += '<div class="col-xs-12 text-center choose-color-heading color-heading-tumb">Избери цвят</div><br /><br />';
            for (var lc = 0; lc < laptop.colors.length; lc++) {
                var laptop_color = laptop.colors[lc];
                color[laptop_color.id] = laptop_color.color;
                value[laptop_color.id] = 0;
                html += "<div class='col-xs-3 col-centered'>";
                html += '<img class="color img-responisve img-thumbnail img-thumb-color center-block" data-id="' + laptop.id + '" data-color="' + laptop_color.color + '" data-colorid="' + laptop_color.id + '" src="' + laptop_color.img + '" width="100">';
                html += '<p class="text-center color-heading-tumb">' + laptop_color.text + '</p>';
                html += "</div>";
            }
            html += '</div>';
        }
        html += '</div>';
        html += "<div class=\"col-xs-12 col-sm-12 col-md-5 col-lg-5 aspire5-description-container\">";
        html += "<ul class='text-left list list-unstyled landing-description'>";
        html += "<li class='cpu-info'><span>Процесор</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info hidden-lg hidden-md' data-placement='top' data-toggle='tooltip' title='" + laptop.cpu + "' data-original-title='test'></span><br /> <p class='info-mobile-align'>" + laptop.cpu + "</p></li>";
        html += "<li><span>Памет</span><br /> <p class='info-mobile-align description-ram-" + laptop.id + "'>" + laptop.ram + "</p></li>";
        html += "<li><span>Видеокарта</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info xs-view-only' data-placement='top' data-toggle='tooltip' title='" + laptop.vga + "' data-original-title=''></span><br /> <p class='info-mobile-align'>" + laptop.vga + "</p></li>";
        html += "<li><span>SSD</span><div> <p class='info-mobile-align description-ssd-" + laptop.id + "'>" + laptop.ssd + "</p></div></li>";

        html += "<li><span>Хардк диск</span><br /> <p class='info-mobile-align'>" + laptop.hdd + "</p></li>";
        html += "</ul>";
        html += "<div class='choose-upgrade'>";
        html += '<i aria-hidden="true" class="fa fa-hdd-o hdd-acer">&nbsp;</i> <span class="choose-model-aspire5">Избери UPGRADE</span>';
        html += '</div>';
        html += "<div class='button-section'>";
        html += '<br /><ul class="nav nav-tabs text-left text-sm-center" role="tablist">';


        for (var v = 0; v < variations.length; v++) {
            var ssd_variation = variations[v];

            var ram = typeof (ssd_variation.label_ram) != 'undefined' ? ssd_variation.label_ram : '';
            var hdd = typeof (ssd_variation.label_hdd) != 'undefined' ? ssd_variation.label_hdd : '';
            var ssd = typeof (ssd_variation.label_ssd) != 'undefined' ? ssd_variation.label_ssd : ssd_variation.label;

            html += '<li class="' + (v == 0 ? 'active' : '') + '" role="presentation"><a aria-controls="home" data-price="' + ssd_variation.price + '" data-id="' + laptop.id + '" data-hdd="' + hdd + '" data-ram="' + ram + '" data-ssd="' + ssd + '" data-value="' + ssd_variation.ssd + '" data-toggle="tab" href="#p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tab">' + ssd_variation.label + '</a></li>';

            //if(ssd_variation.colors.length>1) {
            for (var c = 0; c < ssd_variation.colors.length; c++) {
                var ssd_variation_color = ssd_variation.colors[c];
                products['' + laptop.id + ssd_variation_color.color + ssd_variation.ssd + ''] = ssd_variation_color.id;
            }
            //}
            if (ssd_variation.colors.length == 1) {
                products['' + laptop.id + 'default' + ssd_variation.ssd + ''] = parseInt(ssd_variation.link.substr(1));
            }

        }
        html += '</ul>';
        if (laptop.add_windows == true) {
            html += '<div class="checkbox"><label><input type="checkbox" name="windows" class="windows" id="windows-' + laptop.id + '" data-id="' + laptop.id + '"/>' + laptop.windows + '</label>';
        }
        html += '<div class="tab-content tab-content-' + laptop.id + ' custom-tab-content">';
        for (var v = 0; v < variations.length; v++) {
            var ssd_variation = variations[v];
            html += ' <div class="tab-pane ' + (v == 0 ? 'active' : '') + '" id="p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tabpanel"><br />';
            html += '<div class="row custom-buttonWrapper-checkout">';
            html += '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 custom-price-wrapper"><span class="text-left price-tag-label">' + ssd_variation.price + '</span><span class="lv-tag">лв.</span></div>';

            html += '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 custom-buyNow-wrapper"><a href="' + ssd_variation.link + '">КУПИ СЕГА&nbsp;</a></div>';
            html += '</div>';
            html += '</div>';
        }
        html += "</div>";
        html += "</div>";
        html += '</div>';
        html += "</div>";
        html += "</div>";
    }
    /*PC END*/

    /*Laptop START*/
    if (laptop.categoryItem == 'laptop') {
        var variations = laptop.variations;
        var html = '<div class="row product-cust-containers product-container product-container-' + laptop.id + '" style="display:none;">';
        html += "<div class=\"col-xs-12 col-md-7 col-lg-7\">";
        html += "<h3 class='color-heading text-center'>" + laptop.name + "</h3>";
        html += "<img src='" + laptop.img + "' class=\"img-responsive big-img-" + laptop.id + "\">";

        if (laptop.colors.length > 1) {
            html += '<div class="row thumbnail-container text-center">';
            html += '<div class="col-xs-12 text-center choose-color-heading color-heading-tumb">Избери цвят</div><br /><br />';
            for (var lc = 0; lc < laptop.colors.length; lc++) {
                var laptop_color = laptop.colors[lc];
                color[laptop_color.id] = laptop_color.color;
                value[laptop_color.id] = 0;
                html += "<div class='col-xs-3 col-centered'>";
                html += '<img class="color img-responisve img-thumbnail img-thumb-color center-block" data-id="' + laptop.id + '" data-color="' + laptop_color.color + '" data-colorid="' + laptop_color.id + '" src="' + laptop_color.img + '" width="100">';
                html += '<p class="text-center color-heading-tumb">' + laptop_color.text + '</p>';
                html += "</div>";
            }
            html += '</div>';
        }
        html += '</div>';
        html += "<div class=\"col-xs-12 col-sm-12 col-md-5 col-lg-5 aspire5-description-container\">";
        html += "<ul class='text-left list list-unstyled landing-description'>";
        html += "<li class='cpu-info'><span>Процесор</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info hidden-lg hidden-md' data-placement='top' data-toggle='tooltip' title='" + laptop.cpu + "' data-original-title='test'></span><br /> <p class='info-mobile-align'>" + laptop.cpu + "</p></li>";
        html += "<li><span>Екран</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info hidden-lg hidden-md' data-placement='top' data-toggle='tooltip' title='" + laptop.display + "' data-original-title=''></span><br /> <p class='info-mobile-align'>" + laptop.display + "</p> </li>";
        html += "<li><span>Памет</span><br /> <p class='info-mobile-align description-ram-" + laptop.id + "'>" + laptop.ram + "</p></li>";
        html += "<li><span>Видеокарта</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info xs-view-only' data-placement='top' data-toggle='tooltip' title='" + laptop.vga + "' data-original-title=''></span><br /> <p class='info-mobile-align'>" + laptop.vga + "</p></li>";
        html += "<li><span>SSD</span><div> <p class='info-mobile-align description-ssd-" + laptop.id + "'>" + laptop.ssd + "</p></div></li>";

        html += "<li><span>Хардк диск</span><br /> <p class='info-mobile-align'>" + laptop.hdd + "</p></li>";
        html += "</ul>";
        html += "<div class='choose-upgrade'>";
        html += '<i aria-hidden="true" class="fa fa-hdd-o hdd-acer">&nbsp;</i> <span class="choose-model-aspire5">Избери UPGRADE</span>';
        html += '</div>';
        html += "<div class='button-section'>";
        html += '<br /><ul class="nav nav-tabs text-left text-sm-center" role="tablist">';


        for (var v = 0; v < variations.length; v++) {
            var ssd_variation = variations[v];

            var ram = typeof (ssd_variation.label_ram) != 'undefined' ? ssd_variation.label_ram : '';
            var hdd = typeof (ssd_variation.label_hdd) != 'undefined' ? ssd_variation.label_hdd : '';
            var ssd = typeof (ssd_variation.label_ssd) != 'undefined' ? ssd_variation.label_ssd : ssd_variation.label;

            html += '<li class="' + (v == 0 ? 'active' : '') + '" role="presentation"><a aria-controls="home" data-price="' + ssd_variation.price + '" data-id="' + laptop.id + '" data-hdd="' + hdd + '" data-ram="' + ram + '" data-ssd="' + ssd + '" data-value="' + ssd_variation.ssd + '" data-toggle="tab" href="#p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tab">' + ssd_variation.label + '</a></li>';

            //if(ssd_variation.colors.length>1) {
            for (var c = 0; c < ssd_variation.colors.length; c++) {
                var ssd_variation_color = ssd_variation.colors[c];
                products['' + laptop.id + ssd_variation_color.color + ssd_variation.ssd + ''] = ssd_variation_color.id;
            }
            //}
            if (ssd_variation.colors.length == 1) {
                products['' + laptop.id + 'default' + ssd_variation.ssd + ''] = parseInt(ssd_variation.link.substr(1));
            }

        }
        html += '</ul>';
        if (laptop.add_windows == true) {
            html += '<div class="checkbox"><label><input type="checkbox" name="windows" class="windows" id="windows-' + laptop.id + '" data-id="' + laptop.id + '"/>' + laptop.windows + '</label>';
        }
        html += '<div class="tab-content tab-content-' + laptop.id + ' custom-tab-content">';
        for (var v = 0; v < variations.length; v++) {
            var ssd_variation = variations[v];
            html += ' <div class="tab-pane ' + (v == 0 ? 'active' : '') + '" id="p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tabpanel"><br />';
            html += '<div class="row custom-buttonWrapper-checkout">';
            html += '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 custom-price-wrapper"><span class="text-left price-tag-label">' + ssd_variation.price + '</span><span class="lv-tag">лв.</span></div>';

            html += '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 custom-buyNow-wrapper"><a href="' + ssd_variation.link + '">КУПИ СЕГА&nbsp;</a></div>';
            html += '</div>';
            html += '</div>';
        }
        html += "</div>";
        html += "</div>";
        html += '</div>';
        html += "</div>";
        html += "</div>";
    }
    /*Laptops END*/

    /*Tv's START*/
    else if (laptop.categoryItem == 'tv') {
        var html = '<div class="row product-cust-containers product-container product-container-' + laptop.id + '" style="display:none;">';
        html += "<div class=\"col-xs-12 col-md-7 col-lg-7\">";
        html += "<h3 class='color-heading text-center'>" + laptop.name + "</h3>";
        html += "<img src='" + laptop.img + "' class=\"img-responsive big-img-" + laptop.id + "\">";

        if (laptop.colors.length > 1) {
            html += '<div class="row thumbnail-container text-center">';
            html += '<div class="col-xs-12 text-center choose-color-heading color-heading-tumb">Избери цвят</div><br /><br />';
            for (var lc = 0; lc < laptop.colors.length; lc++) {
                var laptop_color = laptop.colors[lc];
                color[laptop_color.id] = laptop_color.color;
                value[laptop_color.id] = 0;
                html += "<div class='col-xs-3 col-centered'>";
                html += '<img class="color img-responisve img-thumbnail img-thumb-color center-block" data-id="' + laptop.id + '" data-color="' + laptop_color.color + '" data-colorid="' + laptop_color.id + '" src="' + laptop_color.img + '" width="100">';
                html += '<p class="text-center color-heading-tumb">' + laptop_color.text + '</p>';
                html += "</div>";
            }
            html += '</div>';
        }
        html += '</div>';
        html += "<div class=\"col-xs-12 col-sm-12 col-md-5 col-lg-5 aspire5-description-container\">";
        html += "<ul class='text-left list list-unstyled landing-description'>";
        html += "<li class='cpu-info'><span>Големина на екрана</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info hidden-lg hidden-md' data-placement='top' data-toggle='tooltip' title='" + laptop.cpu + "' data-original-title='test'></span><br /> <p class='info-mobile-align'>" + laptop.cpu + "</p></li>";
        html += "<li><span>Резолюция</span><br /> <p class='info-mobile-align description-ram-" + laptop.id + "'>" + laptop.ram + "</p></li>";
        html += "<li><span>Честота</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info xs-view-only' data-placement='top' data-toggle='tooltip' title='" + laptop.vga + "' data-original-title=''></span><br /> <p class='info-mobile-align'>" + laptop.vga + "</p></li>";
        html += "<li><span>Технология</span><div> <p class='info-mobile-align description-ssd-" + laptop.id + "'>" + laptop.ssd + "</p></div></li>";

        html += "<li><span>Звук</span><br /> <p class='info-mobile-align'>" + laptop.hdd + "</p></li>";
        html += "</ul>";
        html += "<div class='button-section'>";
        html += '<br /><ul class="nav nav-tabs text-left text-sm-center tv-disable" role="tablist">';

        var variations = laptop.variations;

        for (var v = 0; v < variations.length; v++) {
            var ssd_variation = variations[v];

            var ram = typeof (ssd_variation.label_ram) != 'undefined' ? ssd_variation.label_ram : '';
            var hdd = typeof (ssd_variation.label_hdd) != 'undefined' ? ssd_variation.label_hdd : '';
            var ssd = typeof (ssd_variation.label_ssd) != 'undefined' ? ssd_variation.label_ssd : ssd_variation.label;

            html += '<li class="' + (v == 0 ? 'active' : '') + '" role="presentation"><a aria-controls="home" data-price="' + ssd_variation.price + '" data-id="' + laptop.id + '" data-hdd="' + hdd + '" data-ram="' + ram + '" data-ssd="' + ssd + '" data-value="' + ssd_variation.ssd + '" data-toggle="tab" href="#p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tab">' + ssd_variation.label + '</a></li>';

            //if(ssd_variation.colors.length>1) {
            for (var c = 0; c < ssd_variation.colors.length; c++) {
                var ssd_variation_color = ssd_variation.colors[c];
                products['' + laptop.id + ssd_variation_color.color + ssd_variation.ssd + ''] = ssd_variation_color.id;
            }
            //}
            if (ssd_variation.colors.length == 1) {
                products['' + laptop.id + 'default' + ssd_variation.ssd + ''] = parseInt(ssd_variation.link.substr(1));
            }

        }
        html += '</ul>';
        if (laptop.add_windows == true) {
            html += '<div class="checkbox"><label><input type="checkbox" name="windows" class="windows" id="windows-' + laptop.id + '" data-id="' + laptop.id + '"/>' + laptop.windows + '</label>';
        }
        html += '<div class="tab-content tab-content-' + laptop.id + ' custom-tab-content">';
        for (var v = 0; v < variations.length; v++) {
            var ssd_variation = variations[v];
            html += ' <div class="tab-pane ' + (v == 0 ? 'active' : '') + '" id="p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tabpanel"><br />';
            html += '<div class="row custom-buttonWrapper-checkout">';
            html += '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 custom-price-wrapper"><span class="text-left price-tag-label">' + ssd_variation.price + '</span><span class="lv-tag">лв.</span></div>';

            html += '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 custom-buyNow-wrapper"><a href="' + ssd_variation.link + '">КУПИ СЕГА&nbsp;</a></div>';
            html += '</div>';
            html += '</div>';
        }
        html += "</div>";
        html += "</div>";
        html += '</div>';
        html += "</div>";
        html += "</div>";
    }
    /*tv's products*/

    /*Displays START*/
    else if (laptop.categoryItem == 'display') {
        var html = '<div class="row product-cust-containers product-container product-container-' + laptop.id + '" style="display:none;">';
        html += "<div class=\"col-xs-12 col-md-7 col-lg-7\">";
        html += "<h3 class='color-heading text-center'>" + laptop.name + "</h3>";
        html += "<img src='" + laptop.img + "' class=\"img-responsive big-img-" + laptop.id + "\">";

        if (laptop.colors.length > 1) {
            html += '<div class="row thumbnail-container text-center">';
            html += '<div class="col-xs-12 text-center choose-color-heading color-heading-tumb">Избери цвят</div><br /><br />';
            for (var lc = 0; lc < laptop.colors.length; lc++) {
                var laptop_color = laptop.colors[lc];
                color[laptop_color.id] = laptop_color.color;
                value[laptop_color.id] = 0;
                html += "<div class='col-xs-3 col-centered'>";
                html += '<img class="color img-responisve img-thumbnail img-thumb-color center-block" data-id="' + laptop.id + '" data-color="' + laptop_color.color + '" data-colorid="' + laptop_color.id + '" src="' + laptop_color.img + '" width="100">';
                html += '<p class="text-center color-heading-tumb">' + laptop_color.text + '</p>';
                html += "</div>";
            }
            html += '</div>';
        }
        html += '</div>';
        html += "<div class=\"col-xs-12 col-sm-12 col-md-5 col-lg-5 aspire5-description-container\">";
        html += "<ul class='text-left list list-unstyled landing-description'>";
        html += "<li class='cpu-info'><span>Големина на екрана</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info hidden-lg hidden-md' data-placement='top' data-toggle='tooltip' title='" + laptop.cpu + "' data-original-title='test'></span><br /> <p class='info-mobile-align'>" + laptop.cpu + "</p></li>";
        html += "<li><span>Резолюция</span><br /> <p class='info-mobile-align description-ram-" + laptop.id + "'>" + laptop.ram + "</p></li>";
        html += "<li><span>Честота</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info xs-view-only' data-placement='top' data-toggle='tooltip' title='" + laptop.vga + "' data-original-title=''></span><br /> <p class='info-mobile-align'>" + laptop.vga + "</p></li>";
        html += "<li><span>Технология</span><div> <p class='info-mobile-align description-ssd-" + laptop.id + "'>" + laptop.ssd + "</p></div></li>";

        html += "<li><span>Звук</span><br /> <p class='info-mobile-align'>" + laptop.hdd + "</p></li>";
        html += "</ul>";
        html += "<div class='button-section'>";
        html += '<br /><ul class="nav nav-tabs text-left text-sm-center tv-disable" role="tablist">';

        var variations = laptop.variations;

        for (var v = 0; v < variations.length; v++) {
            var ssd_variation = variations[v];

            var ram = typeof (ssd_variation.label_ram) != 'undefined' ? ssd_variation.label_ram : '';
            var hdd = typeof (ssd_variation.label_hdd) != 'undefined' ? ssd_variation.label_hdd : '';
            var ssd = typeof (ssd_variation.label_ssd) != 'undefined' ? ssd_variation.label_ssd : ssd_variation.label;

            html += '<li class="' + (v == 0 ? 'active' : '') + '" role="presentation"><a aria-controls="home" data-price="' + ssd_variation.price + '" data-id="' + laptop.id + '" data-hdd="' + hdd + '" data-ram="' + ram + '" data-ssd="' + ssd + '" data-value="' + ssd_variation.ssd + '" data-toggle="tab" href="#p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tab">' + ssd_variation.label + '</a></li>';

            //if(ssd_variation.colors.length>1) {
            for (var c = 0; c < ssd_variation.colors.length; c++) {
                var ssd_variation_color = ssd_variation.colors[c];
                products['' + laptop.id + ssd_variation_color.color + ssd_variation.ssd + ''] = ssd_variation_color.id;
            }
            //}
            if (ssd_variation.colors.length == 1) {
                products['' + laptop.id + 'default' + ssd_variation.ssd + ''] = parseInt(ssd_variation.link.substr(1));
            }

        }
        html += '</ul>';
        if (laptop.add_windows == true) {
            html += '<div class="checkbox"><label><input type="checkbox" name="windows" class="windows" id="windows-' + laptop.id + '" data-id="' + laptop.id + '"/>' + laptop.windows + '</label>';
        }
        html += '<div class="tab-content tab-content-' + laptop.id + ' custom-tab-content">';
        for (var v = 0; v < variations.length; v++) {
            var ssd_variation = variations[v];
            html += ' <div class="tab-pane ' + (v == 0 ? 'active' : '') + '" id="p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tabpanel"><br />';
            html += '<div class="row custom-buttonWrapper-checkout">';
            html += '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 custom-price-wrapper"><span class="text-left price-tag-label">' + ssd_variation.price + '</span><span class="lv-tag">лв.</span></div>';

            html += '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 custom-buyNow-wrapper"><a href="' + ssd_variation.link + '">КУПИ СЕГА&nbsp;</a></div>';
            html += '</div>';
            html += '</div>';
        }
        html += "</div>";
        html += "</div>";
        html += '</div>';
        html += "</div>";
        html += "</div>";
    }
    /*Displays Ends here*/

    /*Аccessories START*/
    else if (laptop.categoryItem == 'accessory') {
        var html = '<div class="row product-cust-containers product-container product-container-' + laptop.id + '" style="display:none;">';
        html += "<div class=\"col-xs-12 col-md-7 col-lg-7\">";
        html += "<h3 class='color-heading text-center'>" + laptop.name + "</h3>";
        html += "<img src='" + laptop.img + "' class=\"img-responsive big-img-" + laptop.id + "\">";

        if (laptop.colors.length > 1) {
            html += '<div class="row thumbnail-container text-center">';
            html += '<div class="col-xs-12 text-center choose-color-heading color-heading-tumb">Избери цвят</div><br /><br />';
            for (var lc = 0; lc < laptop.colors.length; lc++) {
                var laptop_color = laptop.colors[lc];
                color[laptop_color.id] = laptop_color.color;
                value[laptop_color.id] = 0;
                html += "<div class='col-xs-3 col-centered'>";
                html += '<img class="color img-responisve img-thumbnail img-thumb-color center-block" data-id="' + laptop.id + '" data-color="' + laptop_color.color + '" data-colorid="' + laptop_color.id + '" src="' + laptop_color.img + '" width="100">';
                html += '<p class="text-center color-heading-tumb">' + laptop_color.text + '</p>';
                html += "</div>";
            }
            html += '</div>';
        }
        html += '</div>';
        html += "<div class=\"col-xs-12 col-sm-12 col-md-5 col-lg-5 aspire5-description-container\">";
        html += "<ul class='text-left list list-unstyled landing-description'>";
        html += "<li><span>Честота</span><span class='glyphicon glyphicon-info-sign mobile-info trans-info xs-view-only' data-placement='top' data-toggle='tooltip' title='" + laptop.vga + "' data-original-title=''></span><br /> <p class='info-mobile-align'>" + laptop.vga + "</p></li>";
        html += "</ul>";
        html += "<div class='button-section'>";
        html += '<br /><ul class="nav nav-tabs text-left text-sm-center tv-disable" role="tablist">';

        var variations = laptop.variations;

        for (var v = 0; v < variations.length; v++) {
            var ssd_variation = variations[v];

            var ram = typeof (ssd_variation.label_ram) != 'undefined' ? ssd_variation.label_ram : '';
            var hdd = typeof (ssd_variation.label_hdd) != 'undefined' ? ssd_variation.label_hdd : '';
            var ssd = typeof (ssd_variation.label_ssd) != 'undefined' ? ssd_variation.label_ssd : ssd_variation.label;

            html += '<li class="' + (v == 0 ? 'active' : '') + '" role="presentation"><a aria-controls="home" data-price="' + ssd_variation.price + '" data-id="' + laptop.id + '" data-hdd="' + hdd + '" data-ram="' + ram + '" data-ssd="' + ssd + '" data-value="' + ssd_variation.ssd + '" data-toggle="tab" href="#p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tab">' + ssd_variation.label + '</a></li>';

            //if(ssd_variation.colors.length>1) {
            for (var c = 0; c < ssd_variation.colors.length; c++) {
                var ssd_variation_color = ssd_variation.colors[c];
                products['' + laptop.id + ssd_variation_color.color + ssd_variation.ssd + ''] = ssd_variation_color.id;
            }
            //}
            if (ssd_variation.colors.length == 1) {
                products['' + laptop.id + 'default' + ssd_variation.ssd + ''] = parseInt(ssd_variation.link.substr(1));
            }

        }
        html += '</ul>';
        if (laptop.add_windows == true) {
            html += '<div class="checkbox"><label><input type="checkbox" name="windows" class="windows" id="windows-' + laptop.id + '" data-id="' + laptop.id + '"/>' + laptop.windows + '</label>';
        }
        html += '<div class="tab-content tab-content-' + laptop.id + ' custom-tab-content">';
        for (var v = 0; v < variations.length; v++) {
            var ssd_variation = variations[v];
            html += ' <div class="tab-pane ' + (v == 0 ? 'active' : '') + '" id="p' + laptop.id + '-v' + ssd_variation.ssd + '" role="tabpanel"><br />';
            html += '<div class="row custom-buttonWrapper-checkout">';
            html += '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 custom-price-wrapper"><span class="text-left price-tag-label">' + ssd_variation.price + '</span><span class="lv-tag">лв.</span></div>';

            html += '<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 custom-buyNow-wrapper"><a href="' + ssd_variation.link + '">КУПИ СЕГА&nbsp;</a></div>';
            html += '</div>';
            html += '</div>';
        }
        html += "</div>";
        html += "</div>";
        html += '</div>';
        html += "</div>";
        html += "</div>";
    }
    /*Аccessories ENDS*/
    $("#products").append(html);
}

$(function () {


    // Categories start
    for (var i = 0; i < categories.length; i++) {
        var category = categories[i];
        console.log(category);
        var cat = '';
        cat += '<div class="col-xs-4 col-sm-4 col-md-4 col-xl-4 category-container category-container-' + category.id + '">';
        cat += '<div class="imgCust-container">';
        cat += '<img src="' + category.image + '" class="img-responsive category-img" data-category="' + category.id + '" />';
        cat += '<div class="textAlignLeft category-img text-left hidden-sm hidden-xs" data-category="' + category.id + '"><img src="/ckeditor_assets/pictures/2946/content_logo-koledna-shapka.png" class="img-responsive"/></div>';
        cat += '<div class="textAlignCenter category-img text-left" data-category="' + category.id + '">' + category.name + '</div>';
        cat += '<div>';
        cat += '</div>';
        $('#categories').append(cat);
    }

    $(document).on('click', '.category-img', function () {

        var categoryID = $(this).data('category');
        console.log(categoryID);
        $('.category-container').hide();
        $('.all-categories-btn').show();
        var category = getObjectByID('category', categoryID)[0];
        console.log(category);
        if (typeof (category) != "undefined") {
            $('.product-container').hide();

            // Banner
            var categoryInfo = '';
            categoryInfo += '<div class="col-xs-6 col-sm-3">';
            categoryInfo += '<img src="' + category.image + '" class="img-responsive category-img" data-category="' + category.id + '" />';
            categoryInfo += '</div>';
            categoryInfo += '<div class="col-xs-6 col-sm-9 text-left">';
            categoryInfo += category.text;
            categoryInfo += '</div>';
            $('#category-info').html(categoryInfo);

            // Filters
            var filters = category.filters;
            var filterBtns = '';
            for (var i = 0; i < filters.length; i++) {
                var filter = filters[i];
                console.log(filter);
                filterBtns += '<div class="col-xs-3 col-sm-1">';
                filterBtns += '<button class="btn category-filter" data-products="' + filter.products + '" data-category="' + category.id + '">' + filter.name + '</button>';
                filterBtns += '</div>';
            }
            $('#category-filters').html(filterBtns);

        }

    });

    $(document).on('click', '.category-filter', function () {
        var categoryID = $(this).data('category');
        console.log(categoryID);
        var products = $(this).data('products').toString();
        console.log(products);
        var productsArr = products.split(',');
        if (typeof (productsArr) != "undefined") {
            console.log(productsArr);
            $('.product-container').hide();
            for (var i = 0; i < productsArr.length; i++) {
                var prd = productsArr[i];
                console.log(prd);
                $('.product-container-' + prd).show();
            }
        }
    });

    $(document).on('click', '.all-categories-btn', function () {
        $(this).hide();
        $('.category-container').show();
        $('#category-info').html('');
        $('#category-filters').html('');
        $('.product-container').hide();
    });

    // Categories end


    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        e.relatedTarget // previous active tab
        setVariant($(e.target));
    });

    function setVariant(el) {
        var ssd = el.data('ssd');
        var ram = el.data('ram');
        var hdd = el.data('hdd');
        var id = el.data('id');
        var price = el.data('price');

        var t = el.data('type');
        var c = color[id];
        var v = el.data('value');
        value[id] = v;
        type[id] = t;
        if (typeof (c) != "undefined") {
            var k = id + '' + '' + c + '' + v;
        }
        else {
            var k = id + 'default' + v;
        }
        if (ssd != '') {
            $('.description-ssd-' + id).html(ssd);
        }
        if (ram != '') {
            $('.description-ram-' + id).html(ram);
        }
        if (hdd != '') {
            $('.description-hdd-' + id).html(hdd);
        }

        // Windows
        var windows = $('#windows-' + id);
        var windowsPrefix = '';
        if (windows.length) {
            if (windows.is(":checked")) {
                console.log('ADD WINDOWS');
                $('.tab-content-' + id + ' .price-tag-label').html(price + 120);
                windowsPrefix = '900';
            }
            else {
                console.log('REMOVE WINDOWS');
                $('.tab-content-' + id + ' .price-tag-label').html(price);
            }
        }
        $('.tab-content-' + id + ' .tab-pane.active a').attr('href', "/products/" + windowsPrefix + products[k]);
    }

    $('.windows').on('click', function () {
        var id = $(this).data('id');
        var price = parseInt($('.tab-content-' + id + ' .price-tag-label').html());
        var href = $('.tab-content-' + id + ' .tab-pane.active a').attr('href');
        var parts = href.split('/');
        var product = parts[parts.length - 1];

        if ($(this).is(":checked")) {
            console.log('ADD WINDOWS');
            console.log($('.tab-content-' + id + ' .price-tag-label').html());
            price = price + 120;
            product = '900' + product;
        }
        else {
            console.log('REMOVE WINDOWS');
            price = price - 120;
            if (product.indexOf('900') == 0) {
                product = product.substr(3);
            }
        }
        $('.tab-content-' + id + ' .price-tag-label').html(price);
        $('.tab-content-' + id + ' .tab-pane.active a').attr('href', '/' + product);
    });

    $('a.variant').on('click', function (e) {
        setVariant($(this));
    });

    // product colors script
    $('.img-thumb-color').on('click', function (e) {
        var colorId = $(this).data('colorid'); // Base color ID
        var id = $(this).data('id'); // Base product ID
        var c = $(this).data('color');
        color[id] = c;
        var k = id + '' + '' + c + '' + value[id];
        var newProductId = products[k];
        if (typeof (newProductId) == 'undefined') {
            newProductId = colorId;
        }
        $('.big-img-' + id).attr('src', $(this).attr('src'));

        // Windows
        var windows = $('#windows-' + id);
        var windowsPrefix = '';
        if (windows.length) {
            if (windows.is(":checked")) {
                console.log('ADD WINDOWS');
                windowsPrefix = '900';
            }
            else {
                console.log('REMOVE WINDOWS');
            }
        }


        $('.tab-content-' + id + ' .tab-pane.active a').attr('href', "/products/" + windowsPrefix + newProductId);
    });
});



function getObjectByID(type, id) {
    if (type == 'laptop') {
        return laptops.filter(
            function (laptops) { return laptops.id == id }
        );
    }
    if (type == 'category') {
        return categories.filter(
            function (categories) { return categories.id == id }
        );
    }
} 
</script>